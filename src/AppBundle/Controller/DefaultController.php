<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/convert-svg", methods={"POST"})
     */
    public function convertSvgAction(Request $request)
    {
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }
        $params = $request->request->get('params');
        $format = $params['format'];
        /**
         * @var UploadedFile $svg
         */
        $svg = $request->files->get('svg');
        $svgXml = file_get_contents($svg->getRealPath());
        $xmlObj = new \SimpleXMLElement($svgXml);
        $params['w'] = $xmlObj['width'];
        $params['h'] = $xmlObj['height'];
        $params['unit'] = '';
        $html = $this->renderView('AppBundle:Default:svg-wrapper.html.twig', [
            'svg' => $svgXml,
            'params' => $params,
        ]);
        $srcPath = $this->getParameter('kernel.project_dir') . '/web/tmp/' . $params['fsName'] . '.html';
        file_put_contents($srcPath, $html);
        $destPath = $this->getParameter('kernel.project_dir') . '/web/images/' . $params['fsName'] . '.' . $format;
        $cmd = '';
        if ($format == 'pdf') {
        } elseif ($format == 'jpg') {
            $cmd = 'wkhtmltoimage'
            . ' --crop-w ' . $params['w']
            . ' --crop-h ' . $params['h']
            . ' ' . $srcPath . ' ' . $destPath;
        }
        exec($cmd, $output, $return);
        $url = $request->getSchemeAndHttpHost() . $request->getBasePath() . '/images/' . $params['fsName'] . '.' . $format;
        $responseBody = ['success' => false, 'url' => $url];
        if (!$return) {
            $responseBody['success'] = true;
        }
        $response = new JsonResponse();
        $response->setContent(json_encode($responseBody, JSON_UNESCAPED_SLASHES));
        return $response;
    }
}
