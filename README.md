img-convert
===========

A service for converting images to various formats

Extra document is resided in directory Docs
```
    proj=/path/to/project
    cd $proj
    composer install
    HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
    sudo setfacl -dR -m u:"$HTTPDUSER":r-X web
    sudo setfacl -R -m u:"$HTTPDUSER":r-X web
    sudo setfacl -dR -m u:"$HTTPDUSER":rwX $proj/var web/tmp web/images
    sudo setfacl -R -m u:"$HTTPDUSER":rwX $proj/var web/tmp web/images
    sudo setfacl -dR -m u:$(whoami):rwX $proj
    sudo setfacl -R -m u:$(whoami):rwX $proj
    rm -rf var/cache/*
    rm -rf var/logs/*
```

Docs/apache-vhost.conf - example of apache virtual host file